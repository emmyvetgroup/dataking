﻿using DataKingCore.Enums;
using DataKingCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataKingCore.Config.PaystackResponse;

namespace DataKingLogic.IHelpers
{
    public interface IPaymentHelper
    {
        Task<bool> CreatePaymentAsync(Payment payment);

        Task<bool> MarkPaymentAsPaid(Guid paymentId);

        Task<bool> MarkPaymentAsPaidCharges(Guid paymentId);

        Task<bool> CreateWallet(string userEmail, decimal amount = 0);

        Task<Wallet> CreateWalletByUserId(string userId, decimal amount = 0);

        Wallet CreateWalletByUserIdNonAsync(string userId, decimal amount = 0);
        
        Task<bool> CreditWallet(string userId, decimal amount, Guid? paymentId);

        Task<bool> CreditWalletDiscount(string userId, decimal amount, Guid? paymentId);

        Task<bool> DebitWalletDiscount(string userId, Product product, decimal amount, Guid? paymentId);

        Task<bool> DebitWallet(string userId, decimal amount, Guid? paymentId);

        Task<bool> RefundWallet(string userId, decimal amount, Guid? paymentId);

        Task<bool> LogWalletHistory(Wallet wallet, decimal amount, TransactionType transactionType, Guid? paymentId);

        Task<PaystackRepsonse> CreatePaystackPayment(int paymentTypeId, string UserId, decimal amount);

        Task<Wallet> GetUserWallet(string userId);

        Wallet GetUserWalletNonAsync(string userId);

        IQueryable<Payment> GetPendingPayments(PaymentStatus paymentStatus);

        Task<List<WalletHistory>> GetUserWalletHistory(string userId);

        Task VerifyPaymentAndCredit(string invoiceNumber, string bearer);

        //public void NewPDFConverter();

        //public void ConvertToPDF();

    }
}
