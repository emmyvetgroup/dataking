﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataKingLogic.IHelpers
{
    public interface ITransitApiHelper
    {
        Task<string> BuyAirtimeAsync(decimal amount, string network, string phoneNumber);
        
        Task<string> BuyDataAsync(string code, string network, string phoneNumber);
    }
}
