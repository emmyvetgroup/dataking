﻿using DataKingCore.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataKingLogic.IHelpers
{
   public interface IUserHelper
    {
        Task<ApplicationUser> FindByUserNameAsync(string username);

        Task<ApplicationUser> FindByPhoneNumberAsync(string phone);

        Task<ApplicationUser> FindByEmailAsync(string email);

        Task<ApplicationUser> FindByIdAsync(string Id);
        ApplicationUser FindById(string Id);

        Task<bool> CreateUsersAsync(ApplicationUser applicationUser);

        Task<UserVerification> CreateUserToken(string userEmail);

        Task<UserVerification> GetUserToken(Guid token);

       Task<bool> MarkTokenAsUsed(UserVerification userVerification);

        Task<bool> RedirectToRolesDashboard(string userId);

        string GetCurrentUserId(string username);

    }
}
