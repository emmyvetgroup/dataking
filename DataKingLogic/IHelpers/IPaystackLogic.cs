﻿using DataKingCore.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using static DataKingCore.Config.PaystackResponse;

namespace DataKingCore.Config
{
    public interface IPaystackLogic
    {
       public PaystackRepsonse MakePayment(Payment payment);
        Task<PaystackRepsonse> VerifyPayment(Payment payment, string Bearer);
        public Paystack GetBy(string reference);
        public Payment ValidatePayment(string reference);

    }
}
