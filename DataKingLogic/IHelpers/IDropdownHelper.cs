﻿using DataKingCore.Enums;
using DataKingCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static DataKingLogic.Helpers.DropdownHelper;

namespace DataKingLogic.IHelpers
{
    public interface IDropdownHelper
    {
        string GetNotification(DropdownEnums adminNotification, bool deleteOption = false);

        Task<CommonDropdowns> GetNewNotification(DropdownEnums adminNotification, bool deleteOption = false);

        Task<List<CommonDropdowns>> GetDropdownByKey(DropdownEnums dropdownKey, bool deleteOption = false);

        List<DropdownEnumModel> GetDropDownEnumsList();

        Task<bool> CreateDropdownsAsync(CommonDropdowns commonDropdown);

        string GetDropDownItemName(int id);
        Task<List<CommonDropdowns>> GetDropdownByKeyId(int dropdownKeyId, bool deleteOption = false);
        Task<bool> CreateProductDropdowns(Product product, CommonDropdowns commonDropdown);

    }
}
