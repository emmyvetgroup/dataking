﻿using DataKingCore.Enums;
using DataKingCore.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DataKingLogic.IHelpers
{
    public interface IPurchaseHelper
    {
        Task<bool> PurchaseAirtimeAsync(decimal amount,Product product, int networkId, string phoneNumber, string userId);
        bool UserCanPay(decimal amount, string currentUserId);

        public Product GetProduct(int productId);

        Task<bool> PurchaseDataAsync(Product product, string phoneNumber, string userId);
    }
}
