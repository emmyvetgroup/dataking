﻿using DataKingCore.DB;
using DataKingCore.Enums;
using DataKingCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DataKingLogic.IHelpers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using static DataKingCore.Config.PaystackResponse;
using DataKingCore.Config;
using DataKingLogic.Services;
using ceTe.DynamicPDF;
using ceTe.DynamicPDF.HtmlConverter;
using ceTe.DynamicPDF.Merger;
using ceTe.DynamicPDF.Cryptography;

namespace DataKingLogic.Helpers
{
    public class PaymentHelper : IPaymentHelper
    {
        private readonly IEmailService _emailService;
        private readonly AppDbContext _context;
        private readonly IUserHelper _userHelper;
        private readonly IGeneralConfiguration _generalConfiguration;
        private readonly IPaystackLogic _paystackLogic;
        private UserManager<ApplicationUser> _userManager;
        public PaymentHelper(IEmailService emailService,AppDbContext context, IUserHelper userHelper, IGeneralConfiguration generalConfiguration , IPaystackLogic paystackLogic , UserManager<ApplicationUser> userManager)
        {
            _emailService = emailService;
            _context = context;
            _userHelper = userHelper;
            _generalConfiguration = generalConfiguration;
            _paystackLogic = paystackLogic;
            _userManager = userManager;
        }

        public async Task<bool> CreatePaymentAsync(Payment payment)
        {

            try
            {
                
                if (payment.PaymentTypeId > 0 && payment.Amount > 0)
                {
                    payment.DateGenerated = DateTime.Now;
                    payment.PaymentStatus = PaymentStatus.Unpaid;
                    payment.InvoiceNumber = GenerateNumber();
                   

                    var newPayment = await _context.AddAsync(payment);
                    await _context.SaveChangesAsync();
                    if (newPayment.Entity.Id != Guid.Empty)
                    {
                        string toEmail = _generalConfiguration.AdminEmail;
                        string subject = " Pos Data Payment ";
                        string message = "Hello, <br> A payment of NGN <b>" + payment.Amount.ToString("G29") + "<b/>" + " <br/> have been made to " + "<b>" + newPayment.Entity.BankAccount.Name + "<b/>" + " , by " + "<b>" + payment.PaidFrom + " <b/>" + ". <br/> Endeavor to confirm the Payment. <br/> Thanks!! ";


                        _emailService.SendEmail(toEmail, subject, message);
                        return true;
                    }


                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PaystackRepsonse> CreatePaystackPayment(int paymentTypeId,string UserId, decimal amount)
        {

            try
            {
                if (paymentTypeId > 0 && amount > 0)
                {
                    var payment = new Payment();
                    payment.DateGenerated = DateTime.Now;
                    payment.PaymentStatus = PaymentStatus.Unpaid;
                    payment.InvoiceNumber = GenerateNumber();
                    payment.PaymentTypeId = paymentTypeId;
                    payment.UserId = UserId;
                    payment.Amount = amount;

                    var newPayment = await _context.AddAsync(payment);
                    await _context.SaveChangesAsync();
                    if (newPayment.Entity.Id != Guid.Empty)
                    {


                        var paystackResponse =  _paystackLogic.MakePayment(newPayment.Entity);

                       Paystack paystack = new Paystack();
                        paystack.Payment = newPayment.Entity;
                        paystack.authorization_url = paystackResponse.data.authorization_url;
                        paystack.access_code = paystackResponse.data.access_code;

                        _context.Paystacks.Add(paystack);

                        await _context.SaveChangesAsync();

                        return paystackResponse;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IQueryable<Payment> GetPendingPayments(PaymentStatus paymentStatus)
        {          
            try
            {
                return _context.Payments?.Where(s => s.PaymentStatus == paymentStatus).Include(s => s.User).Include(s => s.BankAccount).Include(s => s.PaymentType).OrderBy(s => s.DateGenerated);
               
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }     

        public async Task<bool> MarkPaymentAsPaid(Guid paymentId )
        {


            try
            {
                if (paymentId != null && paymentId != Guid.Empty)
                {
                    var payment = await _context.Payments.FindAsync(paymentId);

                    var getUser = _userManager.Users.Where(x => x.Id == payment.UserId)?.FirstOrDefault();

                    string toEmail = getUser.Email;
                    string subject = " Payment Approved ";
                    string message = "Hello " + "<b>" + getUser.UserName + "<b/>" + ", <br> Your payment of NGN " + "<b>" + payment.Amount.ToString("G29") + "<b/>" + " have been approved successfully. " +
                        " Go ahead and make your transactions <br> Thanks!!! ";


                    if (payment != null && payment.Id != null)
                    {
                        payment.PaymentStatus = PaymentStatus.Paid;

                        _context.Update(payment);
                         _context.SaveChanges();
                        await CreditWallet(payment.UserId, payment.Amount, paymentId);

                        _emailService.SendEmail(toEmail, subject, message);
                        return true;
                    }
                   
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> MarkPaymentAsPaidCharges(Guid paymentId)
        {

            try
            {
                if (paymentId != null && paymentId != Guid.Empty)
                {
                    var payment = await _context.Payments.FindAsync(paymentId);
                    if (payment != null && payment.Id != null)
                    {
                        payment.PaymentStatus = PaymentStatus.Paid;

                        _context.Update(payment);
                        _context.SaveChanges();
                        await CreditWalletDiscount(payment.UserId, payment.Amount, paymentId);
                        return true;
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> CreateWallet(string userEmail, decimal amount = 0)
        {

            try
            {
                if (userEmail != null)
                {
                    var user = await _userHelper.FindByEmailAsync(userEmail);
                    if(user != null)
                    {
                        var wallet = GetUserWallet(user.Id);
                        if(wallet == null)
                        {
                            var newWallet = new Wallet()
                            {
                                UserId = user.Id,
                                Balance = amount,
                                LastUpdated = DateTime.Now,
                                
                            };

                            var result = await _context.AddAsync(newWallet);
                            await _context.SaveChangesAsync();

                            if(result.Entity.Id != null)
                            {
                                return true;
                            }
                        }
                    }
                 

                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Wallet> CreateWalletByUserId(string userId, decimal amount = 0)
        {

            try
            {
                if (userId != null)
                {
                    var user = await _userHelper.FindByIdAsync(userId);
                    if (user != null)
                    {
                       
                            var newWallet = new Wallet()
                            {
                                UserId = user.Id,
                                Balance = amount,
                                LastUpdated = DateTime.Now,

                            };

                            var result = await _context.AddAsync(newWallet);
                            await _context.SaveChangesAsync();

                            if (result.Entity.Id != null)
                            {
                                return result.Entity;
                            }
                      
                    }


                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Wallet CreateWalletByUserIdNonAsync(string userId, decimal amount = 0)
        {

            try
            {
                if (userId != null)
                {
                    var user =  _userHelper.FindById(userId);
                    if (user != null)
                    {

                        var newWallet = new Wallet()
                        {
                            UserId = user.Id,
                            Balance = amount,
                            LastUpdated = DateTime.Now,

                        };

                        var result =  _context.Add(newWallet);
                         _context.SaveChanges();

                        if (result.Entity.Id != null)
                        {
                            return result.Entity;
                        }

                    }


                }
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> CreditWallet(string userId, decimal amount, Guid? paymentId)
        {

            try
            {
                if (userId != null && amount > 0)
                {
                   
                     var wallet =  GetUserWalletNonAsync(userId);
                    if (wallet == null)
                    {
                        wallet  =  CreateWalletByUserIdNonAsync(userId, amount);
                        if(wallet.Balance > 0)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        wallet.Balance += amount;
                        _context.Update(wallet);
                        _context.SaveChanges();
                        await LogWalletHistory(wallet, amount, TransactionType.Credit, paymentId);
                        return true;
                       
                    }
                   

                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> CreditWalletDiscount(string userId, decimal amount,  Guid? paymentId )
        {

            try
            {
                if (userId != null && amount > 0)
                {

                    var wallet = GetUserWalletNonAsync(userId);
                    if (wallet == null)
                    {
                        wallet = CreateWalletByUserIdNonAsync(userId, amount);
                        if (wallet.Balance > 0)
                        {
                            return true;

                        }
                    }
                    else
                    {
                        var  price =  1.5m;
                        var pcnt = amount * (price / 100);
                        var total = amount - (pcnt + 100);

                        wallet.Balance += total;
                        _context.Update(wallet);
                        _context.SaveChanges();
                        await LogWalletHistory(wallet, total, TransactionType.Credit, paymentId);
                        return true;

                    }


                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DebitWalletDiscount(string userId,Product product, decimal amount, Guid? paymentId)
        {

            try
            {


                if (userId != null && amount > 0)
                {

                    var wallet = await GetUserWallet(userId);
                    if (wallet != null)
                    {
                        if (wallet.Balance >= amount)
                        {
                            if (product.Discount > 0)
                            {
                                var newAmount = (((product.Discount / 100) * amount)-amount);

                                wallet.Balance -= Math.Abs(newAmount);
                                _context.Update(wallet);
                                await _context.SaveChangesAsync();
                                await LogWalletHistory(wallet, amount, TransactionType.Debit, paymentId);
                                return true;
                            }

                            wallet.Balance -= amount;
                            _context.Update(wallet);
                            await _context.SaveChangesAsync();
                            await LogWalletHistory(wallet, amount, TransactionType.Debit, paymentId);
                            return true;
                        }


                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> DebitWallet(string userId, decimal amount, Guid? paymentId)
        {

            try
            {
                

                if (userId != null && amount > 0)
                {

                    var wallet = await GetUserWallet(userId);
                    if (wallet != null)
                    {
                      if(wallet.Balance >= amount)
                      {
                            wallet.Balance -= amount;
                            _context.Update(wallet);
                            await _context.SaveChangesAsync();
                            await LogWalletHistory(wallet, amount, TransactionType.Debit, paymentId);
                            return true;
                      }
                        

                    }

               }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> RefundWallet(string userId, decimal amount, Guid? paymentId)
        {

            try
            {
                if (userId != null && amount > 0)
                {

                    var wallet = await GetUserWallet(userId);
                    if (wallet != null)
                    {
                        
                            wallet.Balance += amount;
                            _context.Update(wallet);
                            await _context.SaveChangesAsync();
                            await LogWalletHistory(wallet, amount, TransactionType.Refund, paymentId);
                            return true;
                    }

                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> LogWalletHistory(Wallet wallet, decimal amount, TransactionType transactionType, Guid? paymentId)
        {
            try
            {


                if (wallet != null && wallet.UserId != null && amount > 0)
                {
                    WalletHistory history = new WalletHistory()
                    {
                        TransactionType = transactionType,
                        Amount = amount,
                        PaymentId = paymentId != Guid.Empty ? paymentId : null,
                        WalletId = wallet.Id,
                        NewBalance = wallet.Balance,
                        DateOfTransaction = DateTime.Now,

                    };
                    var result =  _context.Add(history);
                     _context.SaveChanges();
                    if (result.Entity.Id != null)
                    {
                        return true;
                    }
                }


                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        
        public async Task <List <WalletHistory>> GetUserWalletHistory(string userId)
        {
            try
            {
                var walletId =  GetUserWallet(userId).Result.Id;
                return await _context.WalletHistories.Where(s => s.WalletId == walletId).Include(s => s.Payment).Include(s => s.Wallet).OrderBy(s => s.DateOfTransaction).ToListAsync();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        } 

        public async Task<Wallet> GetUserWallet(string userId)
        {
            try
            {
                if (userId != null)
                {
                    var wallet = await _context.Wallets.Where(x => x.UserId == userId)?.Include(s => s.User)?.FirstOrDefaultAsync();
                    if (wallet != null && wallet.UserId != null)
                    {
                        return wallet;
                    }
                    else
                    {
                        return await CreateWalletByUserId(userId);
                    }
                   
                }
               
                 return null;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Wallet GetUserWalletNonAsync(string userId)
        {
            try
            {
                if (userId != null)
                {
                    var wallet =  _context.Wallets.Where(x => x.UserId == userId)?.Include(s => s.User)?.FirstOrDefault();
                    if (wallet != null && wallet.UserId != null)
                    {
                        return wallet;
                    }
                    else
                    {
                        return  CreateWalletByUserIdNonAsync(userId);
                    }

                }

                return null;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private string GenerateNumber()
        {
            return DateTime.Now.ToString().ToLower().Replace("am", "").Replace("pm", " ").Replace(":", "").Replace("/", "").Replace(" ", "");
        }

        public async Task VerifyPaymentAndCredit(string invoiceNumber, string bearer)
        {
            var payment =   _context.Payments.Where(s => s.InvoiceNumber == invoiceNumber).FirstOrDefault();

            var response =  await _paystackLogic.VerifyPayment(payment, bearer);
            if (response.status)
            {
                var paymentModel = await MarkPaymentAsPaidCharges(payment.Id);
            }
           
        }


        //public string ConvertToPDF(string )
        //{
        //    Converter.Convert(new Uri("https://en.wikipedia.org"), @"C:\Users\bytem\Downloads\pdfTest\FirstConversion.pdf");



        //    ////Document document = new Document();
        //    ////Template template = new Template();
        //    ////// Add elements to template
        //    ////document.Template = template;
        //    ////Page page1 = new Page(ceTe.DynamicPDF.PageSize.Letter);
        //    ////page1.ApplyDocumentTemplate = false;
        //    ////page1.ApplySectionTemplate = false;
        //    /// MergeDocument lockedFile = new MergeDocument(filePath);
        //    //Aes256Security security = new Aes256Security();
        //    //lockedFile.Security = security;
        //    //lockedFile.Draw(filePath);

        //    //document.Draw(@"C:\Users\bytem\Downloads\pdfTest\convertedFile.pdf");


        //    MergeDocument lockedFile = new MergeDocument(@"C:\Users\bytem\Downloads\pdfTest\FirstConversion.pdf");

        //    Aes256Security security = new Aes256Security("owner", "john");
        //    lockedFile.Security = security;

        //    lockedFile.Draw(@"C:\Users\bytem\Downloads\pdfTest\LockedConversion.pdf");

        //}

        //string toEmail = "john40eze@gmail.com";
        //string subject = " PDF Format ";
        //string message = ConvertToPDF();
        //_emailService.SendEmailWithAttachment(toEmail, subject, message);
        //    return string.Empty;





        //public void NewPDFConverter()
        //{
        //    PageInfo layoutPage = new PageInfo(ceTe.DynamicPDF.PageSize.A4, ceTe.DynamicPDF.PageOrientation.Portrait);
        //    Uri uri = new Uri("https://en.wikipedia.org");

        //    Page page1 = new Page(ceTe.DynamicPDF.PageSize.Letter);
        //    // Add elements to page
        //    page1.ApplyDocumentTemplate = false;
        //    page1.ApplySectionTemplate = false;

        //    HtmlLayout html = new HtmlLayout(uri, layoutPage);
        //    //html.Header.Center.HasPageNumbers = true;           
        //    //html.Footer.Center.HasPageNumbers = true;           

        //    Document document = html.Layout();
        //    document.Pages.Add(page1);
        //    document.Draw(@"C:\Users\bytem\Downloads\pdfTest\LatestConversion.pdf");

        //}



    }
}
