﻿using DataKingCore.Config;
using DataKingCore.DB;
using DataKingLogic.IHelpers;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace DataKingLogic.Helpers
{
    public class TransitApiHelper : ITransitApiHelper
    {
        private readonly IUserHelper _userHelper;
        private readonly IGeneralConfiguration _generalConfiguration;
        public TransitApiHelper (IUserHelper userHelper, IGeneralConfiguration generalConfiguration )
        {
            _userHelper = userHelper;
            _generalConfiguration = generalConfiguration;
        }

       public async Task<string> BuyAirtimeAsync(decimal amount,string code, string phoneNumber)
        {
            try 
            { 
                        using(var client = new HttpClient())
                        {
                            client.BaseAddress = new Uri(_generalConfiguration.TransitUrl);
                                var url = "airtime.php?api_key=" + _generalConfiguration.TransitApiKey +  "&network=" + code +
                                    "&number=" + phoneNumber + "&amount=" + amount.ToString();

                            var response = await client.GetAsync(url);
                             var result = await response.Content.ReadAsStringAsync();
                             return result;
                        }
            
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public async Task<string> BuyDataAsync( string code, string network, string phoneNumber)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(_generalConfiguration.TransitUrl);
                    var url = "data.php?api_key=" + _generalConfiguration.TransitApiKey + "&network=" + network +
                        "&number=" + phoneNumber + "&product_code=" + code;

                    var response = await client.GetAsync(url);
                    var result = await response.Content.ReadAsStringAsync();
                    return result;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}


