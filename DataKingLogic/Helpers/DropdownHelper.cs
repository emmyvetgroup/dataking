﻿using DataKingCore.DB;
using DataKingCore.Enums;
using DataKingCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;
using DataKingLogic.IHelpers;
using System.Threading.Tasks;

namespace DataKingLogic.Helpers
{
    public class DropdownHelper : IDropdownHelper
    {
        private readonly AppDbContext _context;
        public DropdownHelper(AppDbContext context)
        {
            _context = context;
        }
        public async Task<List<CommonDropdowns>> GetDropdownByKey(DropdownEnums dropdownKey, bool deleteOption = false)
        {
            
            var common = new CommonDropdowns()
            {
                Id = 0,
                Name = "-- Select --"
            };
            var dropdowns = await _context.CommonDropdowns.Where(s => s.Deleted == deleteOption && s.DropdownKey == (int)dropdownKey).OrderBy(s => s.Name).ToListAsync();
            dropdowns.Insert(0, common);
            
            return dropdowns;
        }

        public string GetNotification(DropdownEnums adminNotification, bool deleteOption = false)
        {
         
           var dropdowns =  _context.CommonDropdowns.Where(s => s.Deleted == deleteOption && s.DropdownKey == (int)adminNotification)?.FirstOrDefault()?.Name;
           
            return dropdowns;
        }
        public async Task<CommonDropdowns> GetNewNotification(DropdownEnums adminNotification, bool deleteOption = false)
        {

            var newDropdowns = await _context.CommonDropdowns.Where(s => s.Deleted == deleteOption && s.DropdownKey == (int)adminNotification)?.FirstOrDefaultAsync();

            return newDropdowns;
        }
        public List<DropdownEnumModel> GetDropDownEnumsList()
        {
          return ((DropdownEnums[])Enum.GetValues(typeof(DropdownEnums))).Select(c => new DropdownEnumModel() { Id = (int)c, Name = c.ToString() }).Where(x => x.Id != (int) DropdownEnums.AdminNotice).ToList();
        }

        public string GetDropDownItemName(int id) => _context.CommonDropdowns.FindAsync(id).Result?.Name;

        public async Task<bool> CreateDropdownsAsync(CommonDropdowns commonDropdown)
        {
            try
            {
                if (commonDropdown != null && commonDropdown.DropdownKey > 0 && commonDropdown.Name != null)
                {
                    CommonDropdowns newCommonDropdowns = new CommonDropdowns
                    {
                        Name = commonDropdown.Name,
                        DropdownKey = commonDropdown.DropdownKey,
                        Deleted = false,
                        DateCreated = DateTime.Now,
                    };

                    var createdDropdowns = await _context.CommonDropdowns.AddAsync(newCommonDropdowns);

                    await _context.SaveChangesAsync();
                  
                    if (createdDropdowns.Entity.Id > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> CreateProductDropdowns(Product product, CommonDropdowns commonDropdowns)
        {
            try
            {
                if (product != null && product.ParentId > 0 && product.Name != null)
                {
                    Product newProduct = new Product
                    {
                        Name = product.Name,
                        ParentId = product.ParentId,
                        Price = product.Price,
                        Code = product.Code,
                        Active = false,
                        Deleted = false,
                        Discount = product.Discount,
                    };

                    var createdDropdowns = await _context.Products.AddAsync(newProduct);

                    await _context.SaveChangesAsync();
                    if (createdDropdowns.Entity.Id > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public class DropdownEnumModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }

        public async Task<List<CommonDropdowns>> GetDropdownByKeyId(int dropdownKeyId, bool deleteOption = false)
        {

            var common = new CommonDropdowns()
            {
                Id = 0,
                Name = "-- Select --"
                
            };
            var dropdowns = await _context.CommonDropdowns.Where(s => s.Deleted == deleteOption && s.DropdownKey == dropdownKeyId).OrderBy(s => s.Name).ToListAsync();
            dropdowns.Insert(0, common);

            return dropdowns;
        }

    }
}
