﻿using DataKingCore.Config;
using DataKingCore.DB;
using DataKingCore.Enums;
using DataKingCore.Models;
using DataKingLogic.IHelpers;
using DataKingLogic.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataKingLogic.Helpers
{
    public  class PurchaseHelper : IPurchaseHelper
    {
        private readonly AppDbContext _context;
        private readonly IUserHelper _userHelper;
        private readonly IPaymentHelper _paymentHelper;
        private readonly ITransitApiHelper _transitApiHelper;
        private readonly IDropdownHelper _dropdownHelper;
        private readonly IGeneralConfiguration _generalConfiguration;
        private readonly IEmailService _emailService;

        public PurchaseHelper(AppDbContext context, IUserHelper userHelper, IPaymentHelper paymentHelper, ITransitApiHelper transitApiHelper, IDropdownHelper dropdownHelper, IGeneralConfiguration generalConfiguration, IEmailService emailService)
        {
            _context = context;
            _userHelper = userHelper;
            _paymentHelper = paymentHelper;
            _transitApiHelper = transitApiHelper;
            _dropdownHelper = dropdownHelper;
            _generalConfiguration = generalConfiguration;
            _emailService = emailService;
        }
        public bool UserCanPay(decimal amount, string currentUserId)
        {
            return  _paymentHelper.GetUserWallet(currentUserId).Result.Balance >= amount ? true : false;
        }

       

        public async Task<bool>PurchaseAirtimeAsync(decimal amount,Product product, int networkId, string phoneNumber,string userId)
        {
            try
            {
                var debit = await _paymentHelper.DebitWalletDiscount( userId,product, amount, Guid.Empty);
                if (debit)
                {
                    var networkName = _dropdownHelper.GetDropDownItemName(networkId);
                    var airtimeResponse = await _transitApiHelper.BuyAirtimeAsync(amount, product.Code, phoneNumber);
                    if (airtimeResponse.ToLower() == "sent")
                    {
                        return true;
                    }
                    else
                    {
                        _emailService.SendEmail(_generalConfiguration.AdminEmail, "FUND ACCOUNT NOW", "Please fund account now, respnse from API is" + airtimeResponse);

                        await _paymentHelper.RefundWallet(userId, amount, Guid.Empty);
                        return false;
                    }
                }
                return false;
            }
            catch (Exception)
            {
                throw;
            }
        }

    
        public async Task<bool> PurchaseDataAsync(Product product, string phoneNumber, string userId)
        {
            try
            {
                
                var debit = await _paymentHelper.DebitWallet(userId, product.Price, Guid.Empty);
                
                if (debit)
                {
                    var networkName = _dropdownHelper.GetDropDownItemName(product.ParentId);
                    var airtimeResponse = await _transitApiHelper.BuyDataAsync(product.Code, networkName, phoneNumber);
                    if(airtimeResponse.ToLower().Contains("insufficient wallet"))
                    {
                        _emailService.SendEmail(_generalConfiguration.AdminEmail, "FUND ACCOUNT NOW", "Please fund account now......" );
                    }
                    if (airtimeResponse.ToLower() == "sent")
                    {

                        return true;
                    }
                    else
                    {
                        _emailService.SendEmail(_generalConfiguration.AdminEmail, "Error from API", airtimeResponse);


                        await _paymentHelper.RefundWallet(userId, product.Price, Guid.Empty);
                        return false;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public  Product GetProduct( int productId)
        {
            try
            {
                var getProduct = _context.Products.Where(s => !s.Deleted && s.Id == productId).FirstOrDefault();
                if (getProduct != null)
                {
                    return (getProduct);
                }
                return (null);
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
    }
}
