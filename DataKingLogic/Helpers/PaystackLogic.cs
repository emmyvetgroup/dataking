﻿using DataKingCore.Config;
using DataKingCore.DB;
using DataKingCore.Enums;
using DataKingCore.Models;
using DataKingLogic.IHelpers;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using static DataKingCore.Config.PaystackResponse;

namespace DataKingLogic.Helpers
{
    public class PaystackLogic : IPaystackLogic
    {
        

        private RestClient client;
        protected RestRequest request;
        public static string RestUrl = "https://api.paystack.co/";
        
        static string ApiEndPoint = "";
        private AppDbContext _context;
        private IGeneralConfiguration _generalConfiguration;

        public PaystackLogic(AppDbContext contex, IGeneralConfiguration generalConfiguration)
        {
            client = new RestClient(RestUrl);
            _context = contex;        
            _generalConfiguration = generalConfiguration;
           
        }

        public PaystackRepsonse MakePayment(Payment payment)
        {
            PaystackRepsonse paystackRepsonse = null;
           
            try
            {
                long milliseconds = DateTime.Now.Ticks;
                string testid = milliseconds.ToString();
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ApiEndPoint = "/transaction/initialize";
                request = new RestRequest(ApiEndPoint, Method.POST);
                request.AddHeader("accept", "application/json");
                request.AddHeader("Authorization", "Bearer " + _generalConfiguration.PayStakApiKey);
                request.AddParameter("reference", payment.InvoiceNumber);
                //request.AddParameter("transaction_charge", transaction_charge * 100);
                request.AddParameter("amount",payment.Amount * 100);

                List<CustomeField> myCustomfields = new List<CustomeField>();

                CustomeField nameCustomeField = new CustomeField();
                nameCustomeField.display_name = "Email";
                nameCustomeField.variable_name = "Email";
                nameCustomeField.value = payment.User.Email;
                myCustomfields.Add(nameCustomeField);

                Dictionary<string, List<CustomeField>> metadata = new Dictionary<string, List<CustomeField>>();

                metadata.Add("custom_fields", myCustomfields);
                var serializedMetadata = JsonConvert.SerializeObject(metadata);

                request.AddParameter("metadata", serializedMetadata);

                request.AddParameter("email", payment.User.Email);

                var serializedRequest = JsonConvert.SerializeObject(request, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore });

                var result = client.Execute(request);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    paystackRepsonse = JsonConvert.DeserializeObject<PaystackRepsonse>(result.Content);
                }
                return paystackRepsonse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PaystackRepsonse> VerifyPayment(Payment payment, string Bearer)
        {
            PaystackRepsonse paystackRepsonse = null;
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                ApiEndPoint = "/transaction/verify/" + payment.InvoiceNumber;
                request = new RestRequest(ApiEndPoint, Method.GET);
                request.AddHeader("accept", "application/json");
                request.AddHeader("Authorization", "Bearer " + _generalConfiguration.PayStakApiKey);
                var result = client.Execute(request);
                if (result.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    paystackRepsonse = JsonConvert.DeserializeObject<PaystackRepsonse>(result.Content);
                   await Update(paystackRepsonse);
                }
                return paystackRepsonse;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Paystack GetBy(string reference)
        {
            try
            {
                return _context.Paystacks.Where(a => a.Payment.InvoiceNumber == reference).LastOrDefault();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<bool> Update(PaystackRepsonse PaystackRepsonse)
        {
            try
            {
                Expression<Func<Paystack, bool>> selector = p => p.Payment.InvoiceNumber == PaystackRepsonse.data.reference;
                Paystack _paystackEntity = _context.Paystacks.Where(selector).Include(s => s.Payment).OrderBy(s => s.amount).LastOrDefault();
                if (_paystackEntity != null)
                {
                    _paystackEntity.amount = PaystackRepsonse.data.amount;
                    _paystackEntity.bank = PaystackRepsonse.data.authorization.bank;
                    _paystackEntity.brand = PaystackRepsonse.data.authorization.brand;
                    _paystackEntity.card_type = PaystackRepsonse.data.authorization.card_type;
                    _paystackEntity.channel = PaystackRepsonse.data.channel;
                    _paystackEntity.country_code = PaystackRepsonse.data.authorization.country_code;
                    _paystackEntity.currency = PaystackRepsonse.data.currency;
                    _paystackEntity.domain = PaystackRepsonse.data.domain;
                    _paystackEntity.exp_month = PaystackRepsonse.data.authorization.exp_month;
                    _paystackEntity.exp_year = PaystackRepsonse.data.authorization.exp_year;
                    _paystackEntity.fees = PaystackRepsonse.data.fees.ToString();
                    _paystackEntity.gateway_response = PaystackRepsonse.data.gateway_response;
                    _paystackEntity.ip_address = PaystackRepsonse.data.ip_address;
                    _paystackEntity.last4 = PaystackRepsonse.data.authorization.last4;
                    _paystackEntity.message = PaystackRepsonse.message;
                    _paystackEntity.reference = PaystackRepsonse.data.reference;
                    _paystackEntity.reusable = PaystackRepsonse.data.authorization.reusable;
                    _paystackEntity.signature = PaystackRepsonse.data.authorization.signature;
                    _paystackEntity.status = PaystackRepsonse.data.status;
                    _paystackEntity.transaction_date = PaystackRepsonse.data.transaction_date;

                    _context.Update(_paystackEntity);

                     _context.SaveChanges();

                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public Payment ValidatePayment(string reference)
        {
            try
            {
                var details = _context.Paystacks.Where(a => a.Payment.InvoiceNumber == reference).LastOrDefault();

                if (details != null && details.status != null && details.status.Contains("success") && (details.gateway_response.Contains("Approved") || details.gateway_response.Contains("Transaction Successful") || details.gateway_response.Contains("Successful") || details.gateway_response.Contains("Payment successful") || details.gateway_response.Contains("success")) && details.domain == "live")
                {
                 
                    return details.Payment;
                }
               
                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         private DateTime ConvertToDate(string date)
        {
            DateTime newDate = new DateTime();
            try
            {
               
                string[] dateSplit = date.Split('-');
                newDate = new DateTime(Convert.ToInt32(dateSplit[0]), Convert.ToInt32(dateSplit[1]), Convert.ToInt32(dateSplit[2]));
            }
            catch (Exception)
            {
                throw;
            }

            return newDate;
        }

    }
}
