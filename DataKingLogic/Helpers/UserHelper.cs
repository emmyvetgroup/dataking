﻿using DataKingCore.DB;
using DataKingCore.Models;
using DataKingLogic.IHelpers;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataKingLogic.Helpers
{
    public class UserHelper : IUserHelper
    {
        private AppDbContext _context;
        private UserManager<ApplicationUser> _userManager;
        public UserHelper(AppDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<ApplicationUser> FindByUserNameAsync(string username)
        {
            return await _userManager.Users.Where(s => s.UserName == username)?.FirstOrDefaultAsync();
        }

        public string GetCurrentUserId(string username)
        {
            return  _userManager.Users.Where(s => s.UserName == username)?.FirstOrDefaultAsync().Result.Id?.ToString();
        }

        public async Task<ApplicationUser> FindByPhoneNumberAsync(string phoneNumber)
        {
            return await _userManager.Users.Where(s => s.PhoneNumber == phoneNumber)?.FirstOrDefaultAsync();
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email)
        {
            return await _userManager.Users.Where(s => s.Email == email)?.FirstOrDefaultAsync();
        }

        public async Task<ApplicationUser> FindByIdAsync(string Id)
        {
           return await _userManager.Users.Where(s => s.Id == Id)?.FirstOrDefaultAsync();
        }

        public ApplicationUser FindById(string Id)
        {
            return  _userManager.Users.Where(s => s.Id == Id)?.FirstOrDefault();
        }

        public async Task<bool> CreateUsersAsync(ApplicationUser applicationUser)
        {
            try
            {
                if ( applicationUser.GenderId > 0 && applicationUser.Email != null && applicationUser.PhoneNumber != null)
                {

                    var result = await _userManager.CreateAsync(applicationUser,applicationUser.Password);

                    await _context.SaveChangesAsync();

                    return true;
                }    
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<UserVerification> CreateUserToken (string userEmail)
        {
            try
            {              
                var user = await FindByEmailAsync(userEmail);
               if (user != null)
               {             
                    UserVerification userVerification = new UserVerification()
                    {
                        UserId = user.Id,
                    };
                    await _context.AddAsync(userVerification);
                    await _context.SaveChangesAsync();
                    return userVerification;
                }
               else 
                {
                    return null;
                }
                  
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public async Task<UserVerification> GetUserToken(Guid token)
        {
           return await _context.UserVerifications.Where(t => t.Used != true && t.Token == token)?.Include(s =>s.User).FirstOrDefaultAsync();
            
        }

        public async Task<bool> MarkTokenAsUsed(UserVerification userVerification)
        {
               try
               {
                    var VerifiedUser = _context.UserVerifications.Where(s => s.UserId == userVerification.User.Id && s.Used != true).FirstOrDefault();
                    if (VerifiedUser != null)
                    {
                        userVerification.Used = true;
                        userVerification.DateUsed = DateTime.Now;
                        _context.Update(userVerification);
                        await _context.SaveChangesAsync();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
               }

            catch (Exception ex)
            {
                throw ex;
            }
        }

     
        public async Task<bool> RedirectToRolesDashboard (string userId)
        {
            try
            {
                    var currentUser = FindByUserNameAsync(userId);

                    var userDetails = await _userManager.Users.Where(s => s.UserName == currentUser.Result.UserName)?.FirstOrDefaultAsync();                 

                   var goAdmin = await _userManager.IsInRoleAsync(userDetails, "Admin");
                   
                return goAdmin;

            }
           
            catch(Exception exp)
            {
                throw exp;
            }
           
        }


    }   
}

