﻿using DataKing.Models;
using DataKingCore.DB;
using DataKingCore.Enums;
using DataKingCore.Models;
using DataKingLogic.IHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DataKing.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : BaseController
    {
        private readonly IUserHelper _userHelper;
        private readonly IPaymentHelper _paymentHelper;
        private readonly AppDbContext _context;
        private readonly IDropdownHelper _dropdownHelper;    

        public AdminController(IDropdownHelper dropdownHelper, AppDbContext context, IUserHelper userHelper, IPaymentHelper paymentHelper)
        {
            _dropdownHelper = dropdownHelper;
            _userHelper = userHelper;
            _paymentHelper = paymentHelper;
            _context = context;
        }


        public IActionResult Index()
        {
         
;            return View();
        }

        public IActionResult AddDropdown()
        {

            ViewBag.Dropdownkeys = _dropdownHelper.GetDropDownEnumsList();

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddDropdown(CommonDropdowns commonDropdowns)
        {
            ViewBag.Dropdownkeys = _dropdownHelper.GetDropDownEnumsList();
            try
            {
                var status = await _dropdownHelper.CreateDropdownsAsync(commonDropdowns);
                ModelState.Clear();
                if (status)
                {
                    SetMessage("Dropdown Created Successfully", Message.Category.Information);
                }
                else
                {
                    SetMessage("Sorry Unable to Create DropDowns", Message.Category.Error);
                }

                return View();
            }
            catch (Exception)
            {
                SetMessage("Error !" + "Something Went Wrong", Message.Category.Error);
                return View();
            }
        }



        [HttpGet]
        public IActionResult ApprovePayment()
        {
            var pendings = _paymentHelper.GetPendingPayments(PaymentStatus.Unpaid);
            if (pendings != null)
            {
                return View(pendings);
            }
                return View();

        }

        [HttpPost]
        public  async Task <JsonResult> ApprovePayment(Guid paymentId)
        {
            var responseStatus = false;
            try
            {               
               responseStatus = await _paymentHelper.MarkPaymentAsPaid(paymentId);                  

                return Json(responseStatus);
            }
            
            catch (Exception)
            {
               
                return Json(false);
            }
            
        }

        public IActionResult CreateProduct()
        {
          
            ViewBag.Dropdownkeys =  _dropdownHelper.GetDropDownEnumsList();
            return View();
        }


        [HttpPost]
        public async Task<IActionResult> CreateProduct(Product product ,CommonDropdowns commonDropdowns)
        {
            ViewBag.Dropdownkeys = _dropdownHelper.GetDropDownEnumsList();
            try
            {
                var status = await _dropdownHelper.CreateProductDropdowns(product,commonDropdowns);
                ModelState.Clear();
                if (status)
                {
                    SetMessage("Product Dropdown Created Successfully", Message.Category.Information);
                }
                else
                {
                    SetMessage("Sorry Unable to Create DropDowns", Message.Category.Error);
                }

                return View();
            }
            catch (Exception)
            {
                SetMessage("Error !" + "Something Went Wrong", Message.Category.Error);
                return View();
            }
        }

        public async Task<JsonResult> GetProductInDropdown(int id, int dropdownKeyId)
        {
            try
            {
                await _dropdownHelper.GetDropdownByKeyId(dropdownKeyId);

                var getProduct = _context.CommonDropdowns.Where(x => !x.Deleted && x.DropdownKey == id).ToList();

                return Json(new SelectList(getProduct, "Id", "Name"));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        public IActionResult UpdateNotification()
        {
            return View();

        }

        public JsonResult GetNotification()
        {

            try
            {
                var getNotification = _dropdownHelper.GetNotification(DropdownEnums.AdminNotice);
                if (getNotification != null)
                {
                    return Json(getNotification);
                }
                return Json("No Notification Found");
            }
            catch (Exception exp)
            {
                throw exp;
            }

        }

        [HttpPost]
        public async Task <IActionResult>  UpdateNotification(string name)
        {
            try
            {

                var getAllNews =  await _dropdownHelper.GetNewNotification(DropdownEnums.AdminNotice);
                if (getAllNews == null)
                {
                    CommonDropdowns commonDropdowns = new CommonDropdowns();
                    commonDropdowns.Name = name;
                    commonDropdowns.DropdownKey = (int) DropdownEnums.AdminNotice;
                    commonDropdowns.DateCreated = DateTime.Now;

                    _context.CommonDropdowns.Add(commonDropdowns);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }

                getAllNews.Name = name;
                _context.Update(getAllNews);
                _context.SaveChanges();


               
                return RedirectToAction(nameof(Index));
            }
            catch (Exception exp)
            {
                throw exp;
            }
        }


     


    }
}
