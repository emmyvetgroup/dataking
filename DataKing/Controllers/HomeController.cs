﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

using DataKingLogic.Helpers;
using DataKingCore.Enums;
using DataKing.Models;
using DataKingLogic.IHelpers;
using DataKingCore.Models;
using Microsoft.Extensions.Configuration;
using DataKingCore.Config;
using Microsoft.AspNetCore.Authorization;

namespace DataKing.Controllers
{
    

    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IDropdownHelper _dropdownHelper;
        private IConfiguration _configuration;
        private IPaystackLogic _paystackLogic;
        private IPaymentHelper _paymentHelper;
        public HomeController(ILogger<HomeController> logger, IDropdownHelper dropdownHelper, IConfiguration generalConfiguration, IPaystackLogic paystackLogic , IPaymentHelper paymentHelper)
        {
            _logger = logger;
            _dropdownHelper = dropdownHelper;
            _configuration = generalConfiguration;
            _paystackLogic = paystackLogic;
            _paymentHelper = paymentHelper;
        }

        public IActionResult Index()
        {
            return View();
        }

        

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Services()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        public IActionResult PaystackResponse(Paystack paystack)
        {
            string paystackSecrect = _configuration.GetSection("GeneralConfiguration").GetSection("PayStakApiKey").Value;

            _paymentHelper.VerifyPaymentAndCredit(paystack.reference, paystackSecrect);


            return RedirectToAction("Index", "Home", new { Area = "" });
        }

       

    }
}
