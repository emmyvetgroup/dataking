﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataKing.Models;
using Microsoft.AspNetCore.Mvc;

namespace DataKing.Controllers
{
    public class BaseController : Controller
    {
        protected void SetMessage(string message, Message.Category messageType)
        {
            Message msg = new Message(message, (int)messageType);
            TempData["Message"] = msg;
        }
    }
}
