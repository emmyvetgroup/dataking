﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataKing.Models;
using DataKingCore.Config;
using DataKingCore.DB;
using DataKingCore.Enums;
using DataKingCore.Models;
using DataKingLogic.IHelpers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace DataKing.Controllers
{
    [Authorize]

    public class UserController : BaseController
    {
      
        private readonly IUserHelper _userHelper;
        private readonly IPaymentHelper _paymentHelper;
        private readonly AppDbContext _context;
        private readonly IDropdownHelper _dropdownHelper;
        private readonly IPurchaseHelper _purchaseHelper;
        private UserManager<ApplicationUser> _userManager;
        private IConfiguration _configuration;
        private IPaystackLogic _paystackLogic;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public UserController(IPaystackLogic paystackLogic, IDropdownHelper dropdownHelper, AppDbContext context, IUserHelper userHelper,IPaymentHelper paymentHelper, SignInManager<ApplicationUser> signInManager, IPurchaseHelper purchaseHelper, UserManager<ApplicationUser> userManager, IConfiguration configuration)
        {
            _context = context;
            _userHelper = userHelper;
            _dropdownHelper = dropdownHelper;
            _context = context;
            _userHelper = userHelper;
            _paymentHelper = paymentHelper;
            _signInManager = signInManager;
            _purchaseHelper = purchaseHelper;
            _paymentHelper = paymentHelper;
            _configuration = configuration;
            _paystackLogic = paystackLogic;
            _userManager = userManager;

        }


        public async Task<IActionResult> Index()
        {
          
            try
            {
                {
                    var currentUserId = _userHelper.GetCurrentUserId(User.Identity.Name);

                    var userHistory = await _paymentHelper.GetUserWalletHistory(currentUserId);
                    if (userHistory != null)
                    {
                        return View(userHistory);
                    }
                    return View();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        [HttpGet]
        public async Task<IActionResult> Data()
        {
            ViewBag.DataNetwork = await _dropdownHelper.GetDropdownByKey(DropdownEnums.NetworkKey);
            return View();
        }

        
        [HttpPost]
        public async Task<IActionResult> Data(int productId,string phoneNumber)
        {
            var status = false;
            try
            {
                var currentUserId = _userHelper.GetCurrentUserId(User.Identity.Name);
                if (currentUserId != null)
                {
                  var product = _purchaseHelper.GetProduct( productId);
                    if (product != null)
                    {
                        status = await _purchaseHelper.PurchaseDataAsync(product,phoneNumber, currentUserId);
                    }


                }
                 return Json (status);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public JsonResult GetDataBundle(int id)
        {
            try
            {

                var getBundle = _context.Products.Where(x => !x.Deleted && x.ParentId == id).OrderBy(s => s.Price).ToList();
                if(getBundle != null)
                {
                    return Json(new SelectList(getBundle, "Id", "DisplayName"));
                }
                return Json("No Dropdowns Found");
              
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        [HttpGet]
        public async Task<IActionResult> Airtime()
        {
            ViewBag.AirTime = await _dropdownHelper.GetDropdownByKey(DropdownEnums.AirtimeNetwork);
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Airtime( decimal amount, int networkId,  string phoneNumber)
        {
            var status = false;
            try
            {
                var currentUserId = _userHelper.GetCurrentUserId(User.Identity.Name);
                if (currentUserId != null)
                {
                    var product = _context.Products.Where(x => !x.Deleted && x.ParentId == networkId).FirstOrDefault();
                    status  = await _purchaseHelper.PurchaseAirtimeAsync(amount,product, networkId, phoneNumber, currentUserId);
                    
                }
                return Json(status);
            }
            catch (Exception ex)
            {
                throw ex;
               
            }
            
        }
    
        [HttpPost]
        public IActionResult CanPayForItem(decimal amount)
        {
        var status = false;
            try
            {
            var currentUserId = _userHelper.GetCurrentUserId(User.Identity.Name);
            if (currentUserId != null)
            {
                status =  _purchaseHelper.UserCanPay(amount, currentUserId);

            }
            return Json(status);
            }
            catch (Exception)
            {
            throw;

            }

        }
        [HttpPost]
        public IActionResult CanPayForProduct(int productId)
        {
            var status = false;
            try
            {
                var currentUserId = _userHelper.GetCurrentUserId(User.Identity.Name);
                if (currentUserId != null)
                {
                    var product = _purchaseHelper.GetProduct(productId);

                    status = _purchaseHelper.UserCanPay(product.Price, currentUserId);

                }
                return Json(status);
            }
            catch (Exception)
            {
                throw;

            }

        }


        public async Task<IActionResult>  DepositFund()
        {
            ViewBag.PaymentType = await _dropdownHelper.GetDropdownByKey(DropdownEnums.PaymentOptions);
            ViewBag.BankList = await _dropdownHelper.GetDropdownByKey(DropdownEnums.BankList);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> DepositFund(Payment payment)
        {
            ViewBag.PaymentType = await _dropdownHelper.GetDropdownByKey(DropdownEnums.PaymentOptions);
            ViewBag.BankList = await _dropdownHelper.GetDropdownByKey(DropdownEnums.BankList);
            try
            {

                var loggedInUserId = _userHelper.GetCurrentUserId(User.Identity.Name);
                payment.UserId = loggedInUserId;

                var makePayment = await _paymentHelper.CreatePaymentAsync(payment);
                if (makePayment)
                {

                 SetMessage("Payment request submitted, We will verify your payment and top-up your wallet shortly ", Message.Category.Information);  
                    
                }

                  else
                  {
                    SetMessage(" Error In Making Payment, Try Again", Message.Category.Error);                  
                  }
              
                return View();

            }
           

            catch (Exception ex)
            {
                throw ex;
                
            }

        }


        [HttpGet]
        public async Task<IActionResult> TransactionHistory()
        {

           var currentUserId  =   _userHelper.GetCurrentUserId(User.Identity.Name);
           
             var userHistory = await _paymentHelper.GetUserWalletHistory(currentUserId);
            if (userHistory != null)
            {
                return View(userHistory);
            }
            return View();
          
        }

        [HttpGet]
        public async Task<IActionResult> Profile()
        {
            try
            {
                 var currentUserId = _userHelper.GetCurrentUserId(User.Identity.Name);
                
                 var userProfile = await _userManager.Users.Where(s => s.Id == currentUserId).Include(s => s.Gender).ToListAsync();            
     
                 return View(userProfile);            
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        
        public IActionResult PaystackResponse(Paystack paystack)
        {
            string paystackSecrect = _configuration.GetSection("GeneralConfiguration").GetSection("PayStakApiKey").Value;

          _paymentHelper.VerifyPaymentAndCredit( paystack.reference , paystackSecrect);
            

            return RedirectToAction("Index", "Home", new { Area = "" });
        }


        public async Task <IActionResult> PaystackPayment( decimal amount, int paymentTypeId)
        {
            var currentUserId =  _userHelper.GetCurrentUserId(User.Identity.Name);
            if (currentUserId != null)
            {
               var response = await  _paymentHelper.CreatePaystackPayment(paymentTypeId, currentUserId, amount );
                if(response != null)
                {
                 
                 return Json (response.data.authorization_url);

                }

            }
            return null;
        }

        public JsonResult AdminNotification()
        {
            
            try
            {
                var getDropdownName =  _dropdownHelper.GetNotification(DropdownEnums.AdminNotice) ;
             
                if (getDropdownName != null)
                {
                    return Json (getDropdownName);
                }
                return Json("");

            }
            catch(Exception exp)
            {
                throw exp;
            }
        }    

    }
}
