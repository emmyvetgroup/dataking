﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataKing.Models;
using DataKingCore.DB;
using DataKingCore.Enums;
using DataKingCore.Models;
using DataKingCore.ViewModels;
using DataKingLogic.IHelpers;
using DataKingLogic.Services;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace DataKing.Controllers
{
    

    public class AccountController : BaseController
    {
        private readonly IUserHelper _userHelper;
        private readonly IDropdownHelper _dropdownHelper;
        private readonly IEmailService _emailService;
        private readonly IPaymentHelper _paymentHelper;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly AppDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public AccountController(IUserHelper userHelper, IDropdownHelper dropdownHelper, SignInManager<ApplicationUser> signInManager, AppDbContext context, UserManager<ApplicationUser> userManager, IEmailService emailService, IPaymentHelper paymentHelper, RoleManager<IdentityRole> roleManager)
        {
            _signInManager = signInManager;
            _userHelper = userHelper;
            _dropdownHelper = dropdownHelper;
            _context = context;
            _userManager = userManager;
            _emailService = emailService;
            _paymentHelper = paymentHelper;
            _roleManager = roleManager;
        }

        [HttpGet]
        public async Task <IActionResult> Register()
        {
            ViewBag.Gender = await _dropdownHelper.GetDropdownByKey(DropdownEnums.GenderKey);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(ApplicationUser applicationUser)
        {
            ViewBag.Gender = await _dropdownHelper.GetDropdownByKey(DropdownEnums.GenderKey);
            try
            {               
                    var existingUserWithUserName = await _userHelper.FindByUserNameAsync(applicationUser.UserName);
                    if (existingUserWithUserName != null && existingUserWithUserName.Id != null)
                    {
                        SetMessage("UserName already belong to another user", Message.Category.Error);
                        return View(applicationUser);
                    }

                    var existingUserWithPhone = await _userHelper.FindByPhoneNumberAsync(applicationUser.PhoneNumber);
                    if (existingUserWithPhone != null && existingUserWithPhone.Id != null)
                    {
                        SetMessage("Phone Number already belong to another user ", Message.Category.Error);
                        return View(applicationUser);
                        
                        
                    }

                    var existingUserWithEmailAddress = await _userHelper.FindByEmailAsync(applicationUser.Email);
                    if (existingUserWithEmailAddress != null && existingUserWithEmailAddress.Id != null)
                    {
                        SetMessage("Email Address already belong to another user", Message.Category.Error);
                        return View(applicationUser);
                    }

                    var result = await _userHelper.CreateUsersAsync(applicationUser);
                    if (result == true)
                    {
                       await  _paymentHelper.CreateWalletByUserId(applicationUser.Id);
                     await _userManager.AddToRoleAsync(applicationUser, "Users");
                    await _signInManager.SignInAsync(applicationUser, isPersistent: true);
                        return RedirectToAction("Index", "User");
                    }
                return View(applicationUser);
            }
                
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
                return View();
            }
        }


        [HttpGet]
        public IActionResult Login(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Login(ApplicationUser applicationUser, string returnUrl)
        {
            try
            {
                

                //var link = "https://adif.bivisoft.com/";
                ViewData["ReturnUrl"] = returnUrl;
                if (applicationUser.UserName != null && applicationUser.Password != null)
                {
                    var result = await _signInManager.PasswordSignInAsync(applicationUser.UserName, applicationUser.Password, applicationUser.RememberPassword, true);
                    if (result.Succeeded)
                    {
                        var userDetails =     await _userManager.Users.Where(s => s.UserName == applicationUser.UserName)?.FirstOrDefaultAsync();
                //        var pdfMessage = _paymentHelper.NewConvertToPDF(link);

                //        string toEmail = "john40eze@gmail.com";
                //       string subject = " PDF Format ";
                ////string message = "Hello " + "<b>" + getUser.UserName + "<b/>" + ", <br> Your payment of NGN "
                ////+ "<b>" + payment.Amount.ToString("G29") + "<b/>" + " have been approved successfully. " +
                ////    " Go ahead and make your transactions <br> Thanks!!! ";



                        var goAdmin =  await _userManager.IsInRoleAsync(userDetails, "Admin");

                        if (goAdmin)
                        {
                            return RedirectToAction("Index", "Admin");
                        }
                        else
                        {
                            return RedirectToAction("Index", "User");
                        }
                                
                    }
                    else
                    {
                        SetMessage("Invalid UserName or Password", Message.Category.Error);
                        return View();
                    }
                }
                else
                {
                    SetMessage("Fill in the required Fields Please ", Message.Category.Error);
                    return View();
                }
            }

            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
                return View();
            }
        }


        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ApplicationUser applicationUser)
        {
            try
            {
                {
                    var userToken = await _userHelper.CreateUserToken(applicationUser.Email);
                    if (userToken != null)
                    {

                            EmailAddress fromAddress = new EmailAddress()
                            {
                                Name = "Pos Data",
                                Address = "support@posdata.com.ng"
                            };
                            List<EmailAddress> fromAddressList = new List<EmailAddress>
                             {
                                 fromAddress
                             };

                            EmailAddress toAddress = new EmailAddress()
                            {
                                Name = userToken.User.UserName,
                                Address = userToken.User.Email
                            };
                            List<EmailAddress> toAddressList = new List<EmailAddress>
                            {
                                toAddress
                            };
                        string linkToClick = HttpContext.Request.Scheme.ToString() + "://" + HttpContext.Request.Host.ToString() + "/Account/ResetPassword?token=" + userToken.Token;

                        EmailMessage emailMessage = new EmailMessage()
                        {
                            FromAddresses = fromAddressList,
                            ToAddresses = toAddressList,
                            Subject = "PosData Password Reset",
                            Content = "Hi " + userToken.User.UserName + ", <br/> A password reset has been requested for your Pos Data Account, please click on the link below to create a new password <br>" +
                             "<br><a href=" + linkToClick + ">Change Password</a> <br/> or copy the link the link below to your browser </br>" + linkToClick +

                            "<br/>" + "<br> If you have any trouble with your account, you can always email us at support@posdata.com.ng <br> Regards, <br> The PosData team <br> <br> If you didn't register for PosData, please ignore this message.",


                        };
                        _emailService.Send(emailMessage);
                        await _context.SaveChangesAsync();
                        ModelState.Clear();
                        SetMessage("A password reset link has been set to your email address please check your email for the next action.", Message.Category.Information);
                        return View();
                    }
                    else
                    {
                        SetMessage("Email Address you entered does not belong to any account", Message.Category.Error);
                        return View();
                    }

                }
             
            }
            catch (Exception ex)
            {
                throw ex;
               
            }

        }


        [HttpGet]
        public IActionResult ResetPassword(Guid token)
        {
            try
            {
                if (token != null)
                {

                   PasswordResetViewmodel viewmodel = new PasswordResetViewmodel()
                    {
                        Token = token
                    };

                return View(viewmodel);


               }
                else
                {
                    SetMessage("Error! invalid link" , Message.Category.Error);
                }

                return View();
            }
            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
                return View();
            }
        }

        [HttpPost]
        public async Task <IActionResult> ResetPassword(PasswordResetViewmodel viewmodel)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userVerification = await _userHelper.GetUserToken(viewmodel.Token);
                    if (userVerification != null && !userVerification.Used)
                    {
                        await _userManager.RemovePasswordAsync(userVerification.User);
                        await _userManager.AddPasswordAsync(userVerification.User, viewmodel.Password);
                        await _context.SaveChangesAsync();

                        await _userHelper.MarkTokenAsUsed(userVerification);
                        SetMessage("Password Changed Successfully please login to continue", Message.Category.Information);


                        EmailAddress fromAddress = new EmailAddress()
                        {
                            Name = "Password Reset Confirmation",
                            Address = "support@posdata.com.ng"
                        };

                        List<EmailAddress> fromAddressList = new List<EmailAddress>
                        {
                        fromAddress
                        };
                        EmailAddress toAddress = new EmailAddress()
                        {
                            Name = userVerification.User.UserName,
                            Address = userVerification.User.Email,
                        };
                        List<EmailAddress> toAddressList = new List<EmailAddress>
                        {
                            toAddress
                        };

                        EmailMessage emailMessage = new EmailMessage()
                        {
                            FromAddresses = fromAddressList,
                            ToAddresses = toAddressList,
                            Subject = "PosData",
                            Content = "Hi " + userVerification.User.UserName + ", <br/> Your password has been changed." +

                            "<br> If you did not make this change please email us at  " +
                           "support@posdata.com.ng <br> Regards",


                        };
                        _emailService.Send(emailMessage);

                        return View("Login");

                    }
                    else
                    {
                        SetMessage("Sorry! The Link You Entered is Invalid or Expired " , Message.Category.Error);
                        return View();
                    }



                }
                return View();
            }

            catch (Exception ex)
            {
                SetMessage("Error! " + ex.Message, Message.Category.Error);
                return View();
            }
        }

        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> LogOut()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
    }
}
