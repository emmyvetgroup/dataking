﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataKingCore.Migrations
{
    public partial class AddednullablebankAccount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_CommonDropdowns_BankAccountId",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "BankAccountId",
                table: "Payments",
                type: "int",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_CommonDropdowns_BankAccountId",
                table: "Payments",
                column: "BankAccountId",
                principalTable: "CommonDropdowns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_CommonDropdowns_BankAccountId",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "BankAccountId",
                table: "Payments",
                type: "int",
                nullable: false,
                defaultValue: 0,
                oldClrType: typeof(int),
                oldType: "int",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_CommonDropdowns_BankAccountId",
                table: "Payments",
                column: "BankAccountId",
                principalTable: "CommonDropdowns",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
