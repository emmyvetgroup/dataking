﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DataKingCore.Migrations
{
    public partial class AddUserVerificationToDb : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserVerification_AspNetUsers_UserId",
                table: "UserVerification");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserVerification",
                table: "UserVerification");

            migrationBuilder.RenameTable(
                name: "UserVerification",
                newName: "UserVerifications");

            migrationBuilder.RenameIndex(
                name: "IX_UserVerification_UserId",
                table: "UserVerifications",
                newName: "IX_UserVerifications_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserVerifications",
                table: "UserVerifications",
                column: "Token");

            migrationBuilder.AddForeignKey(
                name: "FK_UserVerifications_AspNetUsers_UserId",
                table: "UserVerifications",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_UserVerifications_AspNetUsers_UserId",
                table: "UserVerifications");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserVerifications",
                table: "UserVerifications");

            migrationBuilder.RenameTable(
                name: "UserVerifications",
                newName: "UserVerification");

            migrationBuilder.RenameIndex(
                name: "IX_UserVerifications_UserId",
                table: "UserVerification",
                newName: "IX_UserVerification_UserId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserVerification",
                table: "UserVerification",
                column: "Token");

            migrationBuilder.AddForeignKey(
                name: "FK_UserVerification_AspNetUsers_UserId",
                table: "UserVerification",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
