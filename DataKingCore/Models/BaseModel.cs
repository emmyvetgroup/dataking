﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DataKingCore.Models
{
    public class BaseModel
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }
        public DateTime DateCreated { get; set; }

        //{
        //    get => DateTime.Now;
        //    set {value != null ? value ==  }

        //}
    }
}
