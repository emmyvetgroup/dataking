﻿using DataKingCore.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DataKingCore.Models
{
    public class Payment
    {
        public Guid Id { get; set; }
        public int PaymentTypeId { get; set; }
        [Column(TypeName = "decimal(18,4)")]
        public decimal Amount { get; set; }

        [Display(Name = "Payment")]
        [ForeignKey("PaymentTypeId")]
        public virtual CommonDropdowns PaymentType { get; set; }
        public DateTime DateGenerated { get; set; }

       
        public string UserId { get; set; }
        [Display(Name = "User")]
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }
        public string InvoiceNumber { get; set; }
        public PaymentStatus PaymentStatus { get; set; }

        [Display(Name = "Bank Account Paid To")]
        public int? BankAccountId { get; set; }
        [Display(Name = "Bank Account")]
        [ForeignKey("BankAccountId")]
        public virtual CommonDropdowns BankAccount { get; set; }

        [Display(Name = "Account Name Paid From")]
        public string PaidFrom {get; set;}

       
    }
}
