﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataKingCore.Config
{
   public class GeneralConfiguration :IGeneralConfiguration
    {
        public string TransitApiKey { get; set; }

        public string TransitUrl { get; set; }
        public string PayStakApiKey { get; set; }

        public string AdminEmail { get; set; }
    }
}
