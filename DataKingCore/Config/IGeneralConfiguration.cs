﻿namespace DataKingCore.Config
{
    public interface IGeneralConfiguration
    {
         string TransitApiKey { get; set; }

        string TransitUrl { get; set; }

        string PayStakApiKey { get; set; }

        string AdminEmail { get; set; }
    }
}
