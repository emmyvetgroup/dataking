﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DataKingCore.Enums
{
    public enum TransitApiResponse
    {
      [Description(" If the response shows Sent it means that the request has been successfully delivered")]
      Sent = 1,
      [Description("This means that the wallet balance is not equivalent to the product")]
      InsufficientWallet = 2,
      [Description("This means that there where no access granted due to wrong credentials")]
      AccessDenied = 3,
      [Description("This means that the network entered is not correct")]
      InvalidNetwork = 4,
      [Description("This means that the amount entered is not correct")]
      InvalidAmount = 5,
      [Description("This means that there was no number entered")]
      EmptyNumber = 6,

    }
}
