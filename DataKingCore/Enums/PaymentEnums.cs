﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DataKingCore.Enums
{
     
    public enum TransactionType
    {
        [Description("For Credit")]
        Credit = 1,
        [Description("For Debit")]
        Debit = 2,
        [Description("For Refund")]
        Refund = 3,
       
    }

    public enum PaymentStatus
    {
        [Description("For Unpaid")]
        Unpaid = 1,
        [Description("For Paid")]
        Paid = 2,
        
    }

}
